﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public List<GameObject> enemyList;

    GameObject player;
    PlayerHealth playerHealth;
    public static List<GameObject> enemyGameobjectList = new List<GameObject>();
    private PlayerData gamedataObj;
    string saveFile;

    private void Awake()
    {
        saveFile = Application.persistentDataPath + "/gamedata.json";

        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
    }

    public void OnSaveGameBtnClicked()
    {
        //PlayerPrefs.SetInt("PlayerHealth", playerHealth.CurrentHealth);
        //PlayerPrefs.SetInt("Score", ScoreManager.score);

        gamedataObj = new PlayerData();
        gamedataObj.playerHealth = playerHealth.CurrentHealth;
        gamedataObj.score = ScoreManager.score;
        gamedataObj.playerXpos = player.transform.position.x;
        gamedataObj.playerYpos = player.transform.position.y;
        gamedataObj.playerZpos = player.transform.position.z;

        for (int i = 0; i < enemyGameobjectList.Count; i++)
        {
            gamedataObj.enemyData.Add(new EnemyData());
            gamedataObj.enemyData[i].enemyName = enemyGameobjectList[i].name;
            gamedataObj.enemyData[i].enemyXpos = enemyGameobjectList[i].transform.position.x;
            gamedataObj.enemyData[i].enemyYpos = enemyGameobjectList[i].transform.position.y;
            gamedataObj.enemyData[i].enemyZpos = enemyGameobjectList[i].transform.position.z;
            gamedataObj.enemyData[i].enemyHealth = enemyGameobjectList[i].GetComponent<EnemyHealth>().CurrentHealth;
        }

        string jsonString = JsonUtility.ToJson(gamedataObj);
        File.WriteAllText(saveFile, jsonString);

        PauseManager.inst.canvas.enabled = !PauseManager.inst.canvas.enabled;
        PauseManager.inst.Pause();
        //pauseCanvas.SetActive(false);
    }

    public void OnLoadGameBtnClicked()
    {
        for(int i =0; i < enemyGameobjectList.Count; i++)
        {
            Destroy(enemyGameobjectList[i]);
        }
        enemyGameobjectList.Clear();

        string json = "";

        if (File.Exists(saveFile))
        {
            // Read the entire file and save its contents.
            json = File.ReadAllText(saveFile);
        }

        if (!string.IsNullOrEmpty(json))
        {
            gamedataObj = JsonUtility.FromJson<PlayerData>(json);

            //set player data
            ScoreManager.score = gamedataObj.score;
            playerHealth.healthSlider.value = gamedataObj.playerHealth;
            player.transform.position = new Vector3(gamedataObj.playerXpos, gamedataObj.playerYpos, gamedataObj.playerZpos);

            for (int i = 0; i < gamedataObj.enemyData.Count; i++)
            {
                string enemyName = gamedataObj.enemyData[i].enemyName;
                //GameObject enemy = Resources.Load(enemyName) as GameObject;

                int enemyIndex = enemyList.FindIndex(obj => enemyName.Contains(obj.name));
                print("Enemy Name : " + enemyName);
                print("Enemy Index : " + enemyIndex);
                print("Enemy List Count : " + enemyList.Count);
                if (enemyIndex != -1)
                {                    
                    Vector3 pos = new Vector3(gamedataObj.enemyData[i].enemyXpos, gamedataObj.enemyData[i].enemyYpos, gamedataObj.enemyData[i].enemyZpos);
                    GameObject gb;
                    gb = Instantiate(enemyList[enemyIndex], pos, Quaternion.identity);
                    enemyGameobjectList.Add(gb);
                }                
            }
        }
        PauseManager.inst.canvas.enabled = !PauseManager.inst.canvas.enabled;
        PauseManager.inst.Pause();
        //pauseCanvas.SetActive(false);

        //if (PlayerPrefs.HasKey("PlayerHealth"))
        //{
        //    int health = PlayerPrefs.GetInt("PlayerHealth");
        //    playerHealth.healthSlider.value = health;
        //}

        //if (PlayerPrefs.HasKey("Score"))
        //{
        //    int score = PlayerPrefs.GetInt("Score");
        //    print("Saved Score : " + score);
        //    //ScoreManager.inst.text.text = score.ToString();
        //    ScoreManager.score = score;
        //}
    }
}

[Serializable]
public class PlayerData
{
    public int score;

    public int playerHealth;
    public float playerXpos;
    public float playerYpos;
    public float playerZpos;

    public List<EnemyData> enemyData = new List<EnemyData>();
}

[Serializable]
public class EnemyData
{
    public int enemyHealth;
    public string enemyName;
    public float enemyXpos;
    public float enemyYpos;
    public float enemyZpos;
}