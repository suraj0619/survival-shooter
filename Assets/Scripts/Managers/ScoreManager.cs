﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager inst;

    public static int score;

    [HideInInspector]
    public Text text;
    
    void Awake ()
    {
        inst = this;

        text = GetComponent <Text> ();
        score = 0;
    }

    void Update ()
    {
        text.text = "Score: " + score;
    }
}